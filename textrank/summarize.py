import codecs
from textrank import get_top_k_indices, jaccard_index, pagerank, sort_indices


def concrete(half):
    half.append([])
    result = []
    for i in range(len(half)):
        row = []
        for j in range(i):
            row.append(half[j][i-j-1])
        row.append(1.0)
        row.extend(half[i])
        result.append(row)
    return result


def make_half(lines):
    half = []
    for i in range(len(lines) - 1):
        row = []
        tokens = lines[i].split()
        for j in range(i + 1, len(lines)):
            row.append(jaccard_index(tokens, lines[j].split()))
        half.append(row)
    return half


if __name__ == '__main__':
    with codecs.open('input.txt', 'r', encoding='utf-8') as f:
        lines = [line.strip() for line in f]
        A = concrete(make_half(lines))
        indices = pagerank(len(lines), A)
        with codecs.open('output.txt', 'w', encoding='utf-8') as out:
            for i in sort_indices(get_top_k_indices(indices, 3)):
                out.write(lines[i] + '\n')
