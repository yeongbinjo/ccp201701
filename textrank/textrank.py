import math
import operator
from collections import Counter


def sort_indices(indices):
    indices.sort()
    return indices


def get_top_k_indices(values, k):
    # return indices for top k values, descending order of a value
    copied = list(values)
    copied.sort(reverse=True)
    return [values.index(i) for i in copied[:k]]


def frobenius_norm(two_dimensional_array):
    sum = 0.0
    for row in two_dimensional_array:
        for v in row:
            sum += v * v
    return math.sqrt(sum)


def jaccard_index(sent1, sent2):
    c1 = Counter(sent1)
    c2 = Counter(sent2)
    return sum((c1 & c2).values()) / sum((c1 | c2).values())


def normalize_and_preprocessing(matrix):
    norm_matrix = []
    for row in matrix:
        sum_of_row = sum(row)
        norm_matrix.append([(item / sum_of_row) * 0.85 for item in row])
    return norm_matrix


def transpose(matrix):
    return list(zip(*matrix))


def mult(A, B):
    result = []
    for r in range(len(A)):
        row = []
        for c in range(len(B[0])):
            cur = 0
            for i in range(len(A[0])):
                cur += A[r][i] * B[i][c]
            row.append(cur)
        result.append(row)
    return result


def _calc(A, B, oper):
    result = []
    for r in range(len(A)):
        row = []
        for c in range(len(A[0])):
            row.append(oper(A[r][c], B[r][c]))
        result.append(row)
    return result


def add(A, B):
    return _calc(A, B, operator.add)


def sub(A, B):
    return _calc(A, B, operator.sub)


def pagerank(n, adjacency_matrix):
    M = transpose(normalize_and_preprocessing(adjacency_matrix))
    R = [[1/n] for _ in range(n)]
    P = [[0.15/n] for _ in range(n)]
    while True:
        new_R = add(mult(M, R), P)
        if frobenius_norm(sub(new_R, R)) < 0.000001:
            break
        else:
            R = new_R
    return new_R


if __name__ == '__main__':
    n = eval(input())
    matrix = []
    for _ in range(n):
        matrix.append([eval(token) for token in input().split()])
    for v in pagerank(n, matrix):
        print(v[0])
