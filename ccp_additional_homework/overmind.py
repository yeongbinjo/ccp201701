import math


def check(a, b):
    return not(a[0] == b[0] or a[1] == b[1] or math.fabs(a[0] - b[0]) == math.fabs(a[1] - b[1]))


def make_nodes(row_indices, col_indices):
    nodes = []
    for r in row_indices:
        for c in col_indices:
            nodes.append((r, c))
    return nodes


def is_complete(plan, n):
    print(plan)
    if not plan:
        return False
    row_indices = list(range(n))
    col_indices = list(range(n))
    for node in plan:
        row_indices.remove(node[0])
        col_indices.remove(node[1])
    if len(row_indices) == 0:
        return True
    for target_node in make_nodes(row_indices, col_indices):
        possible = False
        for node in plan:
            if not check(target_node, node):
                possible = True
                break
        if not possible:
            return False
    # print(plan)
    return True


def _is_complete(plan, n):
    if not plan:
        return False
    row_indices = list(range(n))
    col_indices = list(range(n))
    for node in plan:
        row_indices.remove(node[0])
        col_indices.remove(node[1])
    if len(row_indices) == 0:
        return True
    for target_node in make_nodes(row_indices, col_indices):
        possible = False
        for node in plan:
            if _is_diagonal_overlapped(target_node, node):
                possible = True
                break
        if not possible:
            return False
    return True


def simple(n):
    plans = [[]]
    for i in range(n):
        plans = [plan for plan in plans if len(plan) >= i - 2]
        for j in range(n):
            for plan in plans:
                possible = True
                for node in plan:
                    if not check((i, j), node):
                        possible = False
                        break
                if possible:
                    plans.append(plan + [(i, j)])
    plans = [plan for plan in plans if is_complete(plan, n)]
    return len(plans)


def _is_column_overlapped(column, node):
    return node[1] == column


def _is_diagonal_overlapped(a, b):
    return math.fabs(a[0] - b[0]) == math.fabs(a[1] - b[1])


def another(n):
    plans = [[]]
    for r in range(n):
        new_plans = []
        for c in range(n):
            for plan in plans:
                possible = True
                for node in plan:
                    if _is_column_overlapped(c, node) or _is_diagonal_overlapped((r, c), node):
                        possible = False
                        break
                if possible:
                    new_plans.append(plan + [(r, c)])
        plans = new_plans
    plans = [plan for plan in plans if _is_complete(plan, n)]
    return len(plans)


def main(func):
    print(func(int(input())))


def test(func):
    import time
    start = time.time()
    print(func(11))
    end = time.time()
    print(start - end)


if __name__ == '__main__':
    main(another)
