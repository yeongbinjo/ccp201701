

def no_slice(record, indices):
    start, end = _pick_indices(indices)
    zero_exist = False
    one_exist = False
    for i in range(start, end):
        if not one_exist and record[i] == '1':
            if zero_exist:
                return True
            one_exist = True
        if not zero_exist and record[i] == '0':
            if one_exist:
                return True
            zero_exist = True
    return False


def _pick_indices(indices):
    left = indices[0] > indices[1]
    if left:
        start = indices[1]
        end = indices[0] + 1
    else:
        start = indices[0]
        end = indices[1] + 1
    return start, end


def pick_indices(record, indices):
    start, end = _pick_indices(indices)
    token = record[start:end]
    return '1' in token and '0' in token


def better_comparision(record, indices):
    # token이 길기 때문에 더 안좋음
    start = min(indices)
    end = max(indices) + 1
    token = record[start:end]
    total = 0
    for t in token:
        total += 1 if t == '1' else 0
    return 0 < total < (end - start + 1)


def simple(record, indices):
    start = min(indices)
    end = max(indices) + 1
    token = record[start:end]
    return '1' in token and '0' in token


def main(func):
    record = input()
    n = eval(input())
    for _ in range(n):
        if func(record, [int(n) for n in input().split()]):
            print('Yes')
        else:
            print('No')


def __make_record(length):
    import random
    items = ['0', '1']
    return ''.join([items[random.randint(0, 1)] for _ in range(length)])


def __make_questions(record_length, question_length):
    import random
    questions = []
    for _ in range(question_length):
        questions.append([random.randint(0, record_length-1), random.randint(0, record_length-1)])
    return questions


def test(func):
    import time
    record_length = 1000000
    question_length = 99999
    record = __make_record(record_length)
    questions = __make_questions(record_length, question_length)
    start = time.time()
    for question in questions:
        if func(record, question):
            print('Yes')
        else:
            print('No')
    end = time.time()
    print(end - start)


if __name__ == '__main__':
    # test(simple)
    # test(pick_indices)
    # test(better_comparision)
    # test(no_slice)
    # main(simple)
    main(no_slice)
