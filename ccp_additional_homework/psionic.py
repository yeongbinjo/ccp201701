

def simple(n, heights):
    result = [0]
    result_text = '0 '
    for i in range(1, n):
        if heights[i] < heights[i-1]:
            result.append(i)
            result_text += str(i) + ' '
            continue
        target = 0
        for j in range(len(result)-1, -1, -1):
            if heights[i] < heights[result[j]-1]:
                target = result[j]
                break
        result.append(target)
        result_text += str(target) + ' '
    return result_text[:-1]


def main(func):
    n = int(input())
    heights = [int(h) for h in input().split()]
    print(func(n, heights))


def __make_heights(n, max_height):
    import random
    heights = []
    for _ in range(n):
        heights.append(str(random.randint(1, max_height)))
    return heights


def test(func):
    import time
    n = 50000
    max_height = 10000000
    heights = __make_heights(n, max_height)
    start = time.time()
    heights = [int(t) for t in heights]
    print(func(n, heights))
    end = time.time()
    print(end - start)


if __name__ == '__main__':
    # test(simple)
    main(simple)
